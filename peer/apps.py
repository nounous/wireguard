from django.apps import AppConfig
from django.db.models.signals import pre_save


class PeerConfig(AppConfig):
	name = 'peer'

	def ready(self):
		import peer.signals
