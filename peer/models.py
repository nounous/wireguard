import ipaddress
import subprocess

from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.functional import cached_property


class Interface(models.Model):

	name = models.CharField(blank=False, max_length=15, unique=True)

	addresses = ArrayField(models.GenericIPAddressField())

	addresses_lengths = ArrayField(models.PositiveSmallIntegerField())

	listen_port = models.PositiveIntegerField()

	privkey = models.CharField(blank=False, max_length=44)

	@cached_property
	def pubkey(self):
		return subprocess.run(['wg', 'pubkey'], input=self.privkey.encode('ascii'), capture_output=True).stdout.decode('ascii').strip()

	def clean(self):
		if len(self.addresses) != len(self.addresses_lengths):
			raise ValidationError("Addresses and lengths number mismatch.")
		for address, length in zip(self.addresses, self.addresses_lengths):
			try:
				ipaddress.ip_interface(f'{address}/{length}')
			except ValueError:
				raise ValidationError(f"Invalid interface: {address}/{length}.")

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = "interface"
		verbose_name_plural = "interfaces"

class Peer(models.Model):

	user = models.ForeignKey('auth.User', on_delete=models.CASCADE)

	interface = models.ForeignKey(Interface, on_delete=models.CASCADE)

	pubkey = models.CharField(blank=False, max_length=44)

	allowed_ips = ArrayField(models.GenericIPAddressField())

	allowed_ips_lengths = ArrayField(models.PositiveSmallIntegerField())

	def clean(self):
		if len(self.allowed_ips) != len(self.allowed_ips_lengths):
			raise ValidationError("Addresses and lengths number mismatch.")
		for address, length in zip(self.allowed_ips, self.allowed_ips_lengths):
			try:
				ipaddress.ip_interface(f'{address}/{length}')
			except ValueError:
				raise ValidationError(f"Invalid interface: {address}/{length}.")

	def __str__(self):
		return self.pubkey

	class Meta:
		verbose_name = "peer"
		verbose_name_plural = "peers"
