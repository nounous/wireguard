from django.contrib import admin

from .models import Interface, Peer


@admin.register(Interface)
class InterfaceAdmin(admin.ModelAdmin):
	list_display = ('name', 'addresses', 'addresses_lengths', 'listen_port')

@admin.register(Peer)
class PeerAdmin(admin.ModelAdmin):
	list_display = ('user', 'pubkey', 'allowed_ips', 'allowed_ips_lengths')
