import ipaddress
import subprocess

from django.db.models.signals import pre_save
from django.dispatch import receiver

from .models import Interface, Peer


@receiver(pre_save, sender=Interface)
def pre_save_interface(sender, instance, **kwargs):
	try:
		interface = Interface.objects.get(id=instance.id)
		if instance.privkey != interface.privkey:
			raise NotImplementedError("Changing the private key is not supported, please delete the interface and recreate it.")
		old_addresses = set()
		for address, length in zip(interface.addresses, interface.addresses_lengths):
			old_addresses.add(ipaddress.ip_interface(f'{address}/{length}'))
		addresses = set()
		for address, length in zip(instance.addresses, instance.addresses_lengths):
			addresses.add(ipaddress.ip_interface(f'{address}/{length}'))
		to_remove = old_addresses - addresses
		to_add = addresses - old_addresses
		for address in to_remove:
			subprocess.run(['/sbin/ip', 'address', 'delete', str(address), 'dev', interface.name])
		if instance.name != interface.name:
			subprocess.run(['/sbin/ip', 'link', 'set', interface.name, 'name', instance.name])
		for address in to_add:
			subprocess.run(['/sbin/ip', 'address', 'add', str(address), 'dev', instance.name])
	except Interface.DoesNotExist:
		# Create the interface
		subprocess.run(['/sbin/ip', 'link', 'add', 'dev', instance.name, 'type', 'wireguard'])
		for address, length in zip(instance.addresses, instance.addresses_lengths):
			subprocess.run(['/sbin/ip', 'address', 'add', f'{address}/{length}', 'dev', instance.name])
		subprocess.run(['wg', 'set', instance.name, 'listen-port', f'{instance.listen_port}', 'private-key', '/dev/stdin'], input=instance.privkey.encode('ascii'))
		subprocess.run(['/sbin/ip', 'link', 'set', instance.name, 'up'])

def post_delete_interface(sender, instance, **kwargs):
	subprocess.run(['/sbin/ip', 'link', 'delete', instance.name])


def peer_on_create(peer):
	subprocess.run(['wg', 'set', peer.interface.name, 'peer', peer.pubkey, 'allowed-ips', ','.join([f'{address}/{length}' for address, length in zip(interface.allowed_ips, interface.allowed_ips_lengths)])])
