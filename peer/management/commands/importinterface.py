
import json
import subprocess

from django.core.management.base import BaseCommand, CommandError

from peer.models import Interface


class Command(BaseCommand):
	help = "Import interfaces from OS to database."

	def add_argument(self, parser):
		parser.add_argument('interface', nargs='?', default='all')

	def handle(self, *args, **options):
		if options['interface'] == 'all':
			for interface in json.loads(subprocess.run(['/sbin/ip', 'address', 'show'], capture_output=True).stdout.decode('ascii')):
				if 'POINTOPOINT' not in interface['flags'] or interface['link_type'] != 'none':
					# This is not a WireGuard interface
					continue
				# Check if it is a WireGuard interface
				wg = subprocess.run(['wg', 'showconf', interface['ifname']], capture_output=True)
				if wg.returncode:
					continue
				conf = wg.stdout.decode('ascii')
				privkey = None
				listen_port = None
				for line in conf.split('\n'):
					if line.startswith('PrivateKey = '):
						privkey = line[len('PrivateKey = '):]
					if line.startswith('ListenPort = '):
						privkey = line[len('ListenPort = '):]
				if privkey is None or listen_port is None:
					continue
				addresses = []
				addresses_lengths = []
				for address in interface['addr_info']:
					addresses.append(address['local'])
					addresses_lengths.append(address['prefixlen'])
				try:
					db_interface = Interface.objects.get(name=interface['ifname'])
					raise NotImplementedError("Interface already exists in database.")
				except Interface.DoesNotExist:
					db_interface = Interface(name=interface['ifname'], addresses=addresses, addresses_lengths=addresses_lengths, listen_port=listen_port, privkey=privkey)
					db_interface.save()
